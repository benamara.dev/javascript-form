const form = document.querySelector('form');
const inputs = document.querySelectorAll('input[type="text"], input[type="password"] ');
const progressBar = document.getElementById("progress-bar");

let pseudo, email, password, confirmPass; //backend

const errorChecker = function (tag, message, valid) {
    const container = document.querySelector("." + tag + "-container");
    const errorSpan = document.querySelector("." + tag + "-container > span");

    if (!valid) {
        container.classList.add("error");
        errorSpan.textContent = message;
    } else {
        container.classList.remove("error");
        errorSpan.textContent = message;
    }

}

const pseudoChecker = function (elPseudo) {

    if (elPseudo.length > 0 && (elPseudo.length < 3 || elPseudo.length > 20) ) {
        errorChecker("pseudo","le pseudo doit faire entre 3 et 20 caracteres");
        pseudo = null;
    } else if (!elPseudo.match(/^[a-zA-Z0-9_.-]*$/)) {
        errorChecker("pseudo","le pseudo ne doit pas contenir de caracteres speciaux" );
        pseudo = null;
    } else{
        errorChecker("pseudo","", true );
        pseudo = elPseudo;
    }

}

const emailChecker = function (elEmail) {

    if (!elEmail.match(/^[\w_-]+@[\w-]+\.[a-z]{2,4}$/i)) {
        errorChecker("email", "l'email n'est pas valide" );
        email = null;

    } else {
        errorChecker("email", "", true );
        email = elEmail;
    }
}


const passwordChecker = function (elPassword) {
    progressBar.classList = ""; // progresses ne s ecrasent pas

    if (!elPassword.match(/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/)) {
        errorChecker(
            "password",
            "Minimum de 8 catactères, une majuscule, une minuscule, un chiffre, un caractère spécial"
        );
        progressBar.classList.add("progressRed");
        password = null;


    }
    // else if ( elPassword.length < 12 ) {
    //     progressBar.classList.add("progressBlue");
    //     errorChecker("password", "", true );
    //     password = elPassword;
    // }
    else {
        progressBar.classList.add("progressGreen");
        errorChecker("password", "", true );
        password = elPassword;
    }

    if (confirmPass) {
        confirmChecker(confirmPass)
    }

    

};

let confirmChecker = function (elConfirme) {
    if (elConfirme !== password) {
        errorChecker(
            "confirm",
            "Les mots de passe ne correspondent pas"
        );
        confirmPass = false;
    } else {
        errorChecker(
            "confirm",
            "",
            true,
        );
        confirmPass = true;
    }
}

inputs.forEach(
    (boxInput)=>{
        boxInput.addEventListener('input', function (e) {


            if (e.target.id == "pseudo") {
               // console.log("pseudochecker");
                pseudoChecker(e.target.value);
            } else if (e.target.id == "email")  {
                emailChecker(e.target.value);
            } else if (e.target.id == "password")  {
                passwordChecker(e.target.value);
            } else if (e.target.id == "confirm"){
                confirmChecker(e.target.value);
            }

        })
    }
);

form.addEventListener('submit', function (e) {
    e.preventDefault();
    if (pseudo && email && password && confirmPass) {
        const dataOrApi = {
            pseudo : pseudo,
            email : email,
            password : password,
            confirmPass : confirmPass,
        };
        console.log(dataOrApi);

        progressBar.classList = ""; //remove progress bar
        inputs.forEach(
            (input)=>input.elPseudo=""
        ) // A FAIRE: renommer elConfirme.elEmail.elPassword.elPseudo -> value

        pseudo = null; //delete
        email = null;
        password = null;
        confirmPass = null;



    } else {
        alert ("Veuillez remplir le formulaire")
    }
});