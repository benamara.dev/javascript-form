const inputs = document.querySelectorAll("input");


let pseudoChecker = function (e) {
   // console.log(e);

    const pseudoContainer = document.querySelector('.pseudo-container');
    const errorPseudo = document.querySelector('.pseudo-container > span');

    if (e.length>0 && (e.length<3 || e.length>20) ) {
        pseudoContainer.classList.add('error');
        errorPseudo.textContent = "le pseudo doit faire entre 3 et 20 caracteres";
    } else if (!e.match(/^[a-zA-Z0-9_.-]*$/)) {
        pseudoContainer.classList.add('error');
        errorPseudo.textContent = "le pseudo ne doit pas contenir de caracteres speciaux";
    } else{
        pseudoContainer.classList.remove('error');
        errorPseudo.textContent = "";
    }

}
let emailChecker = function (e) {
    console.log(e);
}
let passwordChecker = function (e) {
    console.log(e);
}
let confirmChecker = function (e) {
    console.log(e);
}

inputs.forEach(
    (boxInput)=>{
        boxInput.addEventListener('input', function (e) {


            if (e.target.id == "pseudo") {
               // console.log("pseudochecker");
                pseudoChecker(e.target.value);
            } else if (e.target.id == "email")  {
                emailChecker(e.target.value);
            } else if (e.target.id == "password")  {
                passwordChecker(e.target.value);
            } else if (e.target.id == "confirm"){
                confirmChecker(e.target.value);
            }

        })
    }
)